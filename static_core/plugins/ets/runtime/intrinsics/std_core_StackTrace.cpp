
/**
 * Copyright (c) 2021-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "runtime/runtime_helpers.h"
#include "plugins/ets/runtime/ets_coroutine.h"
#include "plugins/ets/runtime/ets_exceptions.h"
#include "plugins/ets/runtime/ets_panda_file_items.h"
#include "plugins/ets/runtime/ets_vm.h"
#include "plugins/ets/runtime/types/ets_string.h"
#include "runtime/include/thread_scopes.h"

#include "runtime/include/stack_walker.h"
#include "runtime/include/thread.h"
#include "runtime/interpreter/runtime_interface.h"
#include "runtime/handle_scope.h"
#include "runtime/handle_scope-inl.h"

namespace panda::ets::intrinsics {

namespace {
const char *STACK_TRACE_ELEMENT_CLASS_DESC = "Lstd/core/StackTraceElement;";
const char *STACK_TRACE_ELEMENT_FIELD_CLASS_NAME_NAME = "className";             // synchronize with ETS class
const char *STACK_TRACE_ELEMENT_FIELD_METHOD_NAME_NAME = "methodName";           // synchronize with ETS class
const char *STACK_TRACE_ELEMENT_FIELD_SOURCE_FILE_NAME_NAME = "sourceFileName";  // synchronize with ETS class
const char *STACK_TRACE_ELEMENT_FIELD_LINE_NUMBER_NAME = "lineNumber";           // synchronize with ETS class
const char *STACK_TRACE_ELEMENT_FIELD_RESULT_STRING_NAME = "resultString";       // synchronize with ETS class
}  // namespace

extern "C" EtsArray *StdCoreStackTraceProvisionStackTrace()
{
    auto coroutine = EtsCoroutine::GetCurrent();
    auto vm = coroutine->GetPandaVM();
    [[maybe_unused]] EtsHandleScope scope(coroutine);

    auto linker = vm->GetClassLinker();
    auto stack_trace_element_class = linker->GetClass(STACK_TRACE_ELEMENT_CLASS_DESC);

    auto thread = ManagedThread::GetCurrent();
    auto walker = StackWalker::Create(thread);

    EtsField *class_name_field =
        stack_trace_element_class->GetDeclaredFieldIDByName(STACK_TRACE_ELEMENT_FIELD_CLASS_NAME_NAME);
    EtsField *method_name_field =
        stack_trace_element_class->GetDeclaredFieldIDByName(STACK_TRACE_ELEMENT_FIELD_METHOD_NAME_NAME);
    EtsField *source_file_name_field =
        stack_trace_element_class->GetDeclaredFieldIDByName(STACK_TRACE_ELEMENT_FIELD_SOURCE_FILE_NAME_NAME);
    EtsField *line_number_field =
        stack_trace_element_class->GetDeclaredFieldIDByName(STACK_TRACE_ELEMENT_FIELD_LINE_NUMBER_NAME);
    EtsField *result_string_field =
        stack_trace_element_class->GetDeclaredFieldIDByName(STACK_TRACE_ELEMENT_FIELD_RESULT_STRING_NAME);

    std::vector<std::shared_ptr<EtsHandle<EtsString>>> str_handle_vec;
    std::vector<std::shared_ptr<EtsHandle<EtsObject>>> obj_handle_vec;
    for (auto stack = StackWalker::Create(thread); stack.HasFrame(); stack.NextFrame()) {
        Method *method = stack.GetMethod();

        const auto &class_name = method->GetClass()->GetName();
        const auto *method_name = method->GetName().data;
        const auto *source_file = method->GetClassSourceFile().data;
        const auto line_number = method->GetLineNumFromBytecodeOffset(stack.GetBytecodePc());

        if (method_name == nullptr) {
            method_name = utf::CStringAsMutf8("<unknown>");
        }

        if (source_file == nullptr) {
            source_file = utf::CStringAsMutf8("<unknown>");
        }

        auto current_ets_stack_trace_line_handle =
            std::make_shared<EtsHandle<EtsObject>>(coroutine, EtsObject::Create(stack_trace_element_class));
        obj_handle_vec.push_back(current_ets_stack_trace_line_handle);

        auto class_name_value =
            std::make_shared<EtsHandle<EtsString>>(coroutine, EtsString::CreateFromMUtf8(class_name.c_str()));
        current_ets_stack_trace_line_handle->GetPtr()->SetFieldObject(class_name_field,
                                                                      class_name_value->GetPtr()->AsObject());

        auto method_name_value = std::make_shared<EtsHandle<EtsString>>(
            coroutine, EtsString::CreateFromMUtf8(reinterpret_cast<const char *>(method_name)));
        current_ets_stack_trace_line_handle->GetPtr()->SetFieldObject(method_name_field,
                                                                      method_name_value->GetPtr()->AsObject());

        auto source_file_value = std::make_shared<EtsHandle<EtsString>>(
            coroutine, EtsString::CreateFromMUtf8(reinterpret_cast<const char *>(source_file)));
        current_ets_stack_trace_line_handle->GetPtr()->SetFieldObject(source_file_name_field,
                                                                      source_file_value->GetPtr()->AsObject());

        auto result_string_value = std::make_shared<EtsHandle<EtsString>>(coroutine, EtsString::CreateFromMUtf8(""));
        current_ets_stack_trace_line_handle->GetPtr()->SetFieldObject(result_string_field,
                                                                      result_string_value->GetPtr()->AsObject());

        current_ets_stack_trace_line_handle->GetPtr()->SetFieldPrimitive(line_number_field, line_number);

        str_handle_vec.push_back(class_name_value);
        str_handle_vec.push_back(method_name_value);
        str_handle_vec.push_back(source_file_value);
    }

    const auto lines_size = static_cast<uint32_t>(obj_handle_vec.size());

    EtsHandle<EtsObjectArray> res_arr_handle(coroutine, EtsObjectArray::Create(stack_trace_element_class, lines_size));

    for (uint32_t i = 0; i < lines_size; i++) {
        res_arr_handle.GetPtr()->Set(i, obj_handle_vec[static_cast<size_t>(i)]->GetPtr());
    }

    return res_arr_handle.GetPtr();
}

}  // namespace panda::ets::intrinsics
